# Monitoring

This project configures the OKD monitoring stack.
See https://gitlab.cern.ch/paas-tools/okd4-install/-/tree/master/docs/components/monitoring/README.md
